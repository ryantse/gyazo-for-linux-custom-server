# Gyazo for Linux (Custom Server)
http://gyazo.com/

### Install

Install Ruby and ImageMagick before installing Gyazo.

    $ sudo apt-get install ruby imagemagick
    $ sudo dpkg -i gyazo_XXX_all.deb

### Add Gyazo to Ubuntu Unity Launcher

1. Open Unity Dash.
2. Search "Gyazo".
3. Drag the Gyazo icon and drop into the launcher.

### Contributions
Gyazo for Linux (Custom Server) is maintained by Ryan Tse and is derived from the Gyazo for Linux released by Nota Inc.

## License

Copyright (c) 2015 Ryan Tse

This software is licensed under GPL.