#!/usr/bin/env ruby

# setting
browser_cmd = 'xdg-open'
clipboard_cmd = 'xclip'

require 'net/http'

hostfile = ENV['HOME'] + "/.gyazo.host"
keyfile = ENV['HOME'] + "/.gyazo.key"

host = ''
key = ''

if File.exist?(hostfile) then
  host = File.read(hostfile).chomp
end

if File.exist?(keyfile) then
  key = File.read(keyfile).chomp
end

# capture png file
tmpfile = "/tmp/image_upload#{$$}.png"
imagefile = ARGV[0]

if imagefile && File.exist?(imagefile) then
  system "convert '#{imagefile}' '#{tmpfile}'"
else
  system "import '#{tmpfile}'"
end

if !File.exist?(tmpfile) then
  exit
end

imagedata = File.read(tmpfile)
File.delete(tmpfile)

# upload
boundary = '----BOUNDARYBOUNDARY----'

CGI = '/upload.php'
UA   = 'Gyazo/1.0'

data = <<EOF
--#{boundary}\r
content-disposition: form-data; name="key"\r
\r
#{key}\r
--#{boundary}\r
content-disposition: form-data; name="imagedata"\r
\r
#{imagedata}\r
--#{boundary}--\r
EOF

header ={
  'Content-Length' => data.length.to_s,
  'Content-type' => "multipart/form-data; boundary=#{boundary}",
  'User-Agent' => UA
}

env = ENV['http_proxy']
if env then
  uri = URI(env)
  proxy_host, proxy_port = uri.host, uri.port
else
  proxy_host, proxy_port = nil, nil
end
Net::HTTP::Proxy(proxy_host, proxy_port).start(host,80) {|http|
  res = http.post(CGI,data,header)
  url = res.response.body
  puts url
  if system "which #{clipboard_cmd} >/dev/null 2>&1" then
    system "echo -n '#{url}' | #{clipboard_cmd}"
  end
  system "#{browser_cmd} '#{url}'"
}
